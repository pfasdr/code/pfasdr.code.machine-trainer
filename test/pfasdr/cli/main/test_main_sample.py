from click.testing import CliRunner

from pfasdr.cli.arguments.determine_grids import determine_grids
from pfasdr.cli.main import main


def test_main_sample():
    # Mock
    runner = CliRunner()
    grid = determine_grids()[0]

    # Test
    result = runner.invoke(main, f'sample {grid}'.split())
    print( result.output)

    # Assert
    assert not result.exception
    assert result.exit_code == 0
    assert result.output.startswith('Sampling grid:  linear_1\n')
