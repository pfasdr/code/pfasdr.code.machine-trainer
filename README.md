#   PFASDR

## Status

[![pipeline status](https://gitlab.com/pfasdr/code/machine-trainer/badges/master/pipeline.svg)](https://gitlab.com/pfasdr/code/machine-trainer/pipelines)
[![coverage report](https://gitlab.com/pfasdr/code/machine-trainer/badges/master/coverage.svg)](https://gitlab.com/pfasdr/code/machine-trainer/-/jobs)

##  OS-level Setup

Ubuntu:

* python3.5-tk
* python3.6-tk
* libgl1-mesa-glx

install them using:

    $ python3 setup.py bootstrap_ubuntu

Windows

*   Go to the releases page on python.org:
    <https://www.python.org/downloads/windows/>.
*   Download the x64 version of the latest Python 3:
    "Windows x86-64 executable installer"
*   Start the installer and choose default options:
    "Install Now"

##  Virtualenv-level Setup

    $ python3.8 -m venv venv
    $ venv/bin/python -m pip install --upgrade pip tox wheel

##  Run Individual Tests

    $ python -m pytest pfasdr/grid/manual/

##  Run Main

    $ PYTHONPATH=. python pfasdr/main/main.py

##  Deep Convolutional GAN

From the Torch DCGAN Tutorial:

    [Generative Adversarial Network](https://arxiv.org/abs/1406.2661) is a generative model that contains a discriminator and a generator.  The discriminator is a binary classifier that is trained to classify the real image as real and the fake image as fake. The discriminator is trained to assign 1 to the real image and 0 to the fake image.The generator is a generative model that creates an image from the latent code. The generator is trained to generate an image that can not be distinguishable from the real image in order to deceive the discriminator.

    In the [Deep Convolutional GAN(DCGAN)](https://arxiv.org/abs/1511.06434), the authors introduce architecture guidlines for stable GAN training. They replace any pooling layers with strided convolutions (for the discriminator) and fractional-strided convolutions (for the generator) and use batchnorm in both the discriminator and the generator. In addition, they use ReLU activation in the generator and LeakyReLU activation in the discriminator. However, in our case, we use LeakyReLU activation in both models to avoid sparse gradients.

    ![alt text](png/dcgan.png)

##  Sample Manual Grid Model

    $ time PYTHONPATH=. python pfasdr/grid/manual/dcgan/0-sample_real_situations.py
    # takes about 12 seconds

##  View The Generated Samples

    $ PYTHONPATH=. python pfasdr/grid/manual/dcgan/1-visualise_real_situations.py

##  Convert Real Situations to Real Images

    $ time PYTHONPATH=. python pfasdr/grid/manual/dcgan/2-real_situation2real_image.py
    # this takes round about 3 minutes

##  View The Real Images

    $ eog ../data/grid/1-real_images/

##  Run DCGAN

    $ time PYTHONPATH=. python pfasdr/grid/manual/dcgan/3-train_model.py
    # this takes around 1 hour

##  Generate Fake Images

    $ time PYTHONPATH=. python pfasdr/grid/manual/dcgan/4-sample_model.py
    # this takes around 5 seconds

##  View Fake Images

    $ eog ../data/grid/3-fake_images/

##  Convert Fake Images to Fake Situations

    $ time PYTHONPATH=. python pfasdr/grid/manual/dcgan/5-fake_image2fake_situation.py  > /dev/null
    # this takes about 2 seconds

##  View The Fake Samples

    $ PYTHONPATH=. python pfasdr/grid/manual/dcgan/6-visualise_fake_situations.py
    # this takes a about 10 seconds to initialize and is dead slow

#   Sequential GAN

##  Convert Real Images to Real Sqeuences

    $ time PYTHONPATH=. python pfasdr/grid/manual/sequential/0-real_situations2real_data.py
    # this takes about 10 seconds

##  Run SeqGAN

    $ time PYTHONPATH=. python pfasdr/grid/manual/sequential/SeqGAN/sequence_gan.py
    # this takes a really long time
    # run over night or something

## Train Neural Network

    $ deactivate && \
    source venv2/bin/activate && \
    PYTHONPATH=. python pfasdr/experiments/DynSim/d_nn_sequential_binary_classificator.py

##  Remove Large Files

     java -Xms512m -Xmx16g -jar ~/Downloads/bfg-1.13.0.jar --massive-non-file-objects-sized-up-to 25M --strip-blobs-bigger-than 30M
     git reflog expire --expire=now --all && git gc --prune=now --aggressive

##  Repository from Directory

    cd ..
    cp -R PFASDR.Code.Main PFASDR.Subproject
    cd PFASDR.Subproject
    java -jar ~/Downloads/bfg-1.13.0.jar --massive-non-file-objects-sized-up-to 25M --delete-folders ./<out of scope directory> --no-blob-protection
    git reflog expire --expire=now --all && git gc --prune=now --aggressive
    git reset HEAD
    rm -rf ./<out of scope directory>

## Installing Compile Time Prerequisites

    sudo apt install --yes libblas-dev liblapack-dev libatlas-base-dev

## Create Runtime Environment

    source venv/bin/activate
     tox -e py37-neural-rocm

## Freeze Runtime Environment Requirements

    .tox/py37-neural-rocm/bin/python -m \
        pip freeze --all --exclude-editable > requirements.d/ze.txt

## Using the Command Line Interface (CLI)

    $ .tox/py37-neural-rocm/bin/python ./pfasdr/cli/main.py sample linear_1
    Sampling grid:  linear_1
    [...]
    $ .tox/py37-neural-rocm/bin/python ./pfasdr/cli/main.py learn linear_1
    Learning grid: linear_1
    [...]
    $ .tox/py37-neural-rocm/bin/python ./pfasdr/cli/main.py monitor
    Learning monitoring learning process ...
    [...]
    $ .tox/py37-neural-rocm/bin/python ./pfasdr/cli/main.py test
    Testing the code base against the test suite ...
    [...]

## Building the Docker Image

    docker build -t registry.gitlab.com/pfasdr/code/machine-trainer .

## Publishing the Docker Image

    docker push registry.gitlab.com/pfasdr/code/machine-trainer

## Using the Docker Image

    docker run --rm -it registry.gitlab.com/pfasdr/code/machine-trainer:latest
