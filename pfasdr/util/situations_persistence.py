import csv
import glob
from os import path

import numpy

SITUATIONS = 'situations'
FIELDNAMES = ['Validity',
              'Tap0', 'Tap1',
              'power0', 'power1', 'power2', 'power3', 'power4', 'power5', 'power6']


def situation_dict_generator(csv_file_path):
    """
    Yield situation dictionaries for each situation of the given file path
    """
    with open(csv_file_path) as csv_file:
        dict_reader = csv.DictReader(csv_file, fieldnames=FIELDNAMES)
        next(dict_reader)  # Skip headers
        for situation_dict in dict_reader:
            yield situation_dict


def situations_dict_generator(date):
    """
    Yield dictionaries for each situation of the given date
    """
    for csv_file_path in glob.glob(path.join(SITUATIONS, date, '*.csv')):
        print('Opening ' + csv_file_path + ' ...')
        for situation_dict in situation_dict_generator(csv_file_path=csv_file_path):
            yield situation_dict


SITUATIONS_DTYPE = {'Validity': numpy.float32,
                    'Tap0': numpy.int64, 'Tap1': numpy.int64,
                    'power0': numpy.float32, 'power1': numpy.float32,
                    'power2': numpy.float32, 'power3': numpy.float32,
                    'power4': numpy.float32, 'power5': numpy.float32,
                    'power6': numpy.float32}

_POWER_COUNT = len([name for name in FIELDNAMES if name not in ['Validity', 'Tap0', 'Tap1']])
_LABEL_COUNT = len(['Validity'])

COLUMN_NAMES_INPUT = tuple(set(FIELDNAMES) - {'Validity', 'Tap0', 'Tap1'})
COLUMN_NAMES_LABELS = ['Validity']

COLUMN_RANGE_INPUT = range(1 + 2, _POWER_COUNT + 2 + 1)
COLUMN_RANGE_OUTPUT = range(0, _LABEL_COUNT)
HEADER_HEIGHT = 1
