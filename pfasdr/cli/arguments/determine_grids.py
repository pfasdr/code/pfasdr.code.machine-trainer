from typing import Tuple


def determine_grids() -> Tuple[str, ...]:
    grids: Tuple[str, ...] = (
        'linear_1', 'linear_2', 'linear_3', 'linear_4', 'linear_5',
        'linear_6', 'linear_7', 'linear_8', 'linear_9', 'linear_10',
        'linear_11', 'linear_12', 'linear_13', 'linear_14', 'linear_15',
        'linear_20', 'linear_50', 'linear_100',
        'mv_oberrhein',
    )

    return grids
