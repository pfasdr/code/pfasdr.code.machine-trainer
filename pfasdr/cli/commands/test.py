import subprocess
import sys

from pfasdr.util.paths import ROOT_PATH


def run_test_suite(
    *,
    skip,
):
    print('Testing the code base against the test suite ...')

    args = f'{sys.executable} -m pytest'
    if skip:
        args += ' --ignore-glob=*test/*'
    print(args)
    args = args.split()

    subprocess.Popen(
        args=args,
        cwd=str(ROOT_PATH),
    ).wait()
