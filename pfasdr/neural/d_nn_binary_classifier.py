import os

from tensorflow.python.keras.callbacks import CSVLogger, ModelCheckpoint, TensorBoard
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, Dropout, LeakyReLU
from tensorflow.python.keras.optimizers import Adam

from share.set_seeds import seed_everything
from pfasdr.neural.d_nn_adaptivity import AdaptLr
from pfasdr.neural.d_nn_util import load_data_sets_from_csv_files
from pfasdr.util.paths import \
    TRAINING_DATA_PROGRESS_FILE_PATH, LAST_CHECKPOINT_PATH


def _main():
    print('Loading data ...')
    x_train, y_train, x_test, y_test = load_data_sets_from_csv_files(
        limit_training=10 ** 7,  # R9 Nano
        limit_testing=10 ** 6,  # R9 Nano
        # limit_training=500000,  # GP100
        # limit_testing=500000,  # GP100
        # file_count=14) # Sampled by Threadripper 1900X
        file_count=6)  # Sampled by ELAB Intel 8 Core

    print('Building model ...')
    model = Sequential()
    model.add(Dense(units=64, input_dim=x_train.shape[1]))
    model.add(LeakyReLU(alpha=0.3))
    model.add(Dropout(0.5))
    model.add(Dense(units=64))
    model.add(LeakyReLU(alpha=0.3))
    model.add(Dropout(0.5))
    model.add(Dense(units=y_train.shape[1],
        activation='sigmoid'))

    logger = CSVLogger(
        append=False,
        filename=TRAINING_DATA_PROGRESS_FILE_PATH)

    checkpointer = ModelCheckpoint(
        filepath=os.path.join(
            LAST_CHECKPOINT_PATH,
            'epoch_{epoch:05d}.hdf5'),
        monitor='val_loss',
        verbose=3,
        save_best_only=False,
        save_weights_only=False,
        mode='auto',
        period=1)

    adapt_lr = AdaptLr(
        monitor='val_loss',
        optimization_mode='min',
        adaption_factor=0.9,
        verbose=1)

    tensorboard = TensorBoard(
        log_dir=LAST_CHECKPOINT_PATH,
        histogram_freq=1,
        # write_graph=True,  # increases log size massively!
        write_graph=False,
        write_grads=True,
        write_images=True)

    # optimizer ='rmsprop'
    optimizer = Adam(
        lr=0.001,
        beta_1=0.9,
        beta_2=0.999,
        epsilon=None,
        decay=0.0,
        amsgrad=False)
    model.compile(
        loss='binary_crossentropy',
        optimizer=optimizer,
        metrics=['accuracy'])

    print('Training model ...')
    history = model.fit(
        x_train, y_train,
        epochs=10 ** 4,
        # batch_size=2**10,               # Threadripper 1900X
        # batch_size=int(1.2 * 2 ** 20),  # R9 Nano, slow convergence
        # batch_size=int(1.4 * 2 ** 11),  # R9 Nano, usable convergence
        #batch_size=10 ** 3,  # GP100
        batch_size=32,  # Laptop
        #callbacks=[checkpointer, logger, adapt_lr],  # No Tensorboard on AMD HipTF
        callbacks=[checkpointer, logger, adapt_lr, tensorboard],
        verbose=1,
        validation_split=0.1)
    try:
        print()
        print(f'historic training loss: {history.history["loss"]}')
        print(f'historic validation loss: {history.history["val_loss"]}')
        print(f'historic training accuracy: {history.history["acc"]}')
        print(f'historic validation accuracy: {history.history["val_acc"]}')
        print()
    except KeyError:  # no [u'"loss"']. Why?
        return
    del history

    print('Testing ...')
    scores = model.evaluate(x_test, y_test,
                            batch_size=100000)
    print()
    for index, name in enumerate(['loss'] + model.metrics):
        print('Testing {name.ljust(8)}: {scores[index]}')
        del index, name
    del scores


if __name__ == '__main__':
    seed_everything()

    print(f'python'
          f'-m tensorboard.main'
          f'--logdir="{LAST_CHECKPOINT_PATH}"')

    _main()
