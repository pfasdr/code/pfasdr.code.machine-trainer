import os
# import shutil
from pathlib import Path

from tensorflow import keras
import pandas as pd
# import tensorflow as tf
# from tensorflow.contrib.learn.python.learn.estimators import debug
# from tensorflow.python.distribute.mirrored_strategy import MirroredStrategy
# from tensorflow.contrib.distribute.python.mirrored_strategy import MirroredStrategy

from pfasdr.neural.d_nn_util import load_valid_data_sets_from_csv_files_normalized, \
    load_invalid_data_sets_from_csv_files_normalized

os.environ['HIP_VISIBLE_DEVICES'] = '-1'

x_train_valid, y_train_valid, x_test_valid, y_test_valid = load_valid_data_sets_from_csv_files_normalized()
x_train_invalid, y_train_invalid, x_test_invalid, y_test_invalid = load_invalid_data_sets_from_csv_files_normalized()

x_train_valid = pd.DataFrame.from_records(x_train_valid)
x_train_invalid = pd.DataFrame.from_records(x_train_invalid)
y_train_valid = pd.DataFrame.from_records(y_train_valid)
y_train_invalid = pd.DataFrame.from_records(y_train_invalid)

x_train = pd.concat((x_train_valid, x_train_invalid), ignore_index=True)
y_train = pd.concat((y_train_valid, y_train_invalid), ignore_index=True)

# y_train = keras.utils.np_utils.to_categorical(y_train, 2)
# y_train = pd.DataFrame.from_records(y_train)

batch_size = 2048
log_dir = Path(__file__).parent / 'zb_log_dir'
print(str(log_dir))
# try:
#     shutil.rmtree(path=log_dir)
# except FileNotFoundError:
#     pass
# os.mkdir(path=log_dir)

# hook = debug.TensorBoardDebugHook("bengt-desktop:8007")

for net_type in ['mlp']:
    for net_depth in [20, 10]:
        for dense_units in [2000, 1000]:
            # model = keras.models.Sequential([
            #    keras.layers.core.Dense(7, input_shape=(7,)),
            #    keras.layers.core.Activation('relu'),
            #    keras.layers.core.Dropout(rate=0.5),
            #    keras.layers.core.Dense(21),
            #    keras.layers.core.Activation('relu'),
            #    keras.layers.core.Dropout(rate=0.5),
            #    keras.layers.core.Activation('sigmoid'),
            #    keras.layers.core.Dense(1),
            # ])

            # strategy = tf.distribute.mirrored_strategy.MirroredStrategy(
            #   cross_device_ops=tf.distribute.HierarchicalCopyAllReduce(),
            #   devices=["/gpu:0", "/gpu:1"]
            # )
            # strategy = tf.distribute.MirroredStrategy()
            # strategy = tf.distribute.OneDeviceStrategy()
            # strategy = tf.distribute.experimental.CentralStorageStrategy()
            # strategy = tf.distribute.Strategy(
            #     extended=
            # )
            # with strategy.scope():

            model = keras.models.Sequential()

            # Configurable Multi-Layer-Perceptron
            model.add(keras.layers.core.Dense(units=dense_units, input_dim=x_train.shape[1], activation='relu'))
            model.add(keras.layers.core.Dropout(rate=0.5))
            for _ in range(net_depth):
                model.add(keras.layers.core.Dense(units=dense_units, activation='relu'))
                model.add(keras.layers.core.Dropout(rate=0.5))
            model.add(keras.layers.core.Dense(1, activation='sigmoid'))

            # optimizer = 'rmsprop'
            optimizer = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
            loss = 'binary_crossentropy'
            metrics = ['accuracy']

            model.compile(
                optimizer=optimizer,
                loss=loss,
                metrics=metrics,
            )

            log_dir_this_run = log_dir / ('cpu_sgd_depth_' + str(net_depth) + '-' + 'units_' + str(dense_units))
            tensorboard_callback = keras.callbacks.TensorBoard(
                log_dir=log_dir_this_run,
                #    histogram_freq=1,
                #    batch_size=32,
                #    write_graph=True,
                #    write_grads=False,
                #    write_images=False,
                #    embeddings_freq=0,
                #    embeddings_layer_names=None,
                #    embeddings_metadata=None,
                #    embeddings_data=None,
                #    update_freq='epoch'
            )
            callbacks = [
                # hook,
                tensorboard_callback,
            ]
            epochs = 50
            model.fit(
                batch_size=batch_size,
                callbacks=callbacks,
                epochs=epochs,
                x=x_train,
                y=y_train,
                validation_split=0.2,
                verbose=True,
            )
