import multiprocessing


def greet(*, greetee):
    return f'Hello, {greetee}!'


greetees = ('Foo', 'Bar')

# greetings = multiprocessing.Pool(2).map(greet, greetees)

# greetings = list(
#     multiprocessing.Pool(2).apply(greet, kwds={'greetee': greetees[i]})
#     for i in range(len(greetees))
# )

from functools import partial
greetings = [
    multiprocessing.Pool(2).apply(
        partial(greet, **{'greetee': greetees[i]})
    )
    for i in range(len(greetees))
]


for greeting in greetings:
    print(greeting)
