from numpy.random.mtrand import seed

from pfasdr.neural.lib_s_minimal_xor.build_model import build_model
from pfasdr.neural.lib_s_minimal_xor.evaluate import evaluate
from pfasdr.neural.lib_s_minimal_xor.mean_squared_error import mean_squared_error
from pfasdr.neural.lib_s_minimal_xor.train_model_on_data import train_model_on_data


def train(*,
          x_train, y_train, x_test, y_test):
    """
    Train a minimal neural network to predict the XOR binary operator.

    :return:
    """
    print('Preprocessing inputs ...')
    samples_training = zip(x_train, y_train)
    samples_evaluation = zip(x_test, y_test)

    print('Storing inputs in memory ...')
    samples_training = list(samples_training)
    samples_evaluation = list(samples_evaluation)

    print("Seeding the random number generator ...")
    random_seed = 0
    seed(random_seed)

    print('Building the model ...')
    # Input count is the feature count
    input_count = x_train[0].shape[0]
    # Hidden node count should be tuned
    hidden_layer_node_count = 20
    # Output count is always only 1 (binary: valid/invalid)
    output_count = 1
    model = build_model(
        hidden_layer_node_count=hidden_layer_node_count,
        input_count=input_count,
        output_count=output_count,
    )

    print('Training the model ...')
    learning_rate = 0.1
    target_error = 0.001
    epoch_limit = 5000
    log_interval_epochs = 1
    error_metric = mean_squared_error
    training_error, evaluation_error = train_model_on_data(
        epoch_limit=epoch_limit,
        error_metric=error_metric,
        learning_rate=learning_rate,
        log_interval_epochs=log_interval_epochs,
        model=model,
        samples_training=samples_training,
        samples_evaluation=samples_evaluation,
        target_error=target_error,
    )

    return training_error, evaluation_error
