from numpy.ma.core import array


def mean_squared_error(errors):
    return (array(errors) ** 2).mean()
