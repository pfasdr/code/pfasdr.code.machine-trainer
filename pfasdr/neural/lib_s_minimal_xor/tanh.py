import numpy as np


# Activation functions
def tanh(x):
    return np.tanh(x)


# Derivative of tanh from its output
def tanh_derivative(y):
    return 1 - y ** 2
