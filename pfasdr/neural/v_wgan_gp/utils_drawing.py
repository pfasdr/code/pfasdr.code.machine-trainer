import numpy as np
from matplotlib import pyplot as plt


def save_scattered_image(*,
                         z, id, z_range_x, z_range_y, name='scattered_image.jpg'):
    # borrowed from https://github.com/ykwon0407/variational_autoencoder/blob/master/variational_bayes.ipynb

    N = 10

    plt.figure(figsize=(8, 6))
    plt.scatter(z[:, 0], z[:, 1], c=np.argmax(id, 1), marker='o', edgecolor='none', cmap=discrete_cmap(N, 'jet'))
    plt.colorbar(ticks=range(N))

    axes = plt.gca()
    axes.set_xlim([-z_range_x, z_range_x])
    axes.set_ylim([-z_range_y, z_range_y])

    plt.grid(True)
    plt.savefig(name)


def discrete_cmap(*,
                  n, base_color_map=None):
    """
    Create an N-bin discrete colormap from the specified input map
    """

    # borrowed from https://gist.github.com/jakevdp/91077b0cae40f8f8244a

    # Note that if base_cmap is a string or None, you can simply do
    #    return plt.cm.get_cmap(base_cmap, N)
    # The following works for string, None, or a colormap instance:

    base = plt.cm.get_cmap(base_color_map)
    color_list = base(np.linspace(0, 1, n))
    cmap_name = base.name + str(n)

    return base.from_list(cmap_name, color_list, n)
