from itertools import count
import os

from tensorflow.python.keras.optimizers import Adam, SGD
from tensorflow.python.keras.callbacks import TensorBoard
from keras_adversarial.adversarial_model import AdversarialModel
from keras_adversarial.adversarial_optimizers import AdversarialOptimizerSimultaneous
from keras_adversarial.adversarial_utils import normal_latent_sampling, gan_targets, simple_gan

# from pfasdr.neural.d_nn_util import load_data_sets_from_csv_files_normalized
from pfasdr.neural.d_nn_util import load_valid_data_sets_from_csv_files_normalized
from pfasdr.neural.w_gan_1d import LATENT_LENGTH, MODEL_SAVE_INTERVAL
from pfasdr.neural.k_model import get_generator_model_directly, get_discriminator_model_directly
from pfasdr.util.paths import CURRENT_CHECKPOINT_PATH


def _main():
    print('python '
          '-m tensorboard.main '
          '--logdir="' + CURRENT_CHECKPOINT_PATH + '"')

    # generator (z -> x)
    generator = get_generator_model_directly()

    # discriminator (x -> y)
    discriminator = get_discriminator_model_directly()

    # gan (x - > yfake, yreal)
    gan = simple_gan(generator, discriminator, normal_latent_sampling((LATENT_LENGTH,)))

    # print summary of models
    generator.summary()
    discriminator.summary()
    gan.summary()

    # build adversarial model
    model = AdversarialModel(
        base_model=gan,  # common base model
        # player_models=[generator, discriminator],
        player_params=[generator.trainable_weights, discriminator.trainable_weights],
        player_names=["generator", "discriminator"])

    model.adversarial_compile(
        adversarial_optimizer=AdversarialOptimizerSimultaneous(),
        player_optimizers=[Adam(lr=0.000001), SGD(lr=0.00001)],
        loss='binary_crossentropy')

    # model.name = model.player_names[0]
    # model.container_nodes = model.layers[0]
    # 'adversarial_model'

    #    model_checkpoint = ModelCheckpoint(
    #        filepath=os.path.join(CURRENT_CHECKPOINT_PATH, 'current_checkpoint.h5'),
    #        monitor='val_loss',
    #        verbose=0,
    #        save_best_only=True,
    #        save_weights_only=False,
    #        mode='auto',
    #        period=1
    #    )

    tensorboard = TensorBoard(
        log_dir=os.path.join(CURRENT_CHECKPOINT_PATH, 'logs'),
        histogram_freq=0,
        # write_graph=True,
        write_images=True)

    x_train, _, x_test, _ = load_valid_data_sets_from_csv_files_normalized()

    # train model
    for step in count(start=0, step=1):
        print('==> Training step ' + str(step) + ' ...')
        # model.fit_generator(
        #    generator=_batch_generator_training(),
        #    # Should be the number of samples divided by the batch size.
        #    steps_per_epoch=int(limit_training / BATCH_SIZE),
        #    epochs=epochs,
        #    verbose=1,
        #    callbacks=[tensorboard],  # , model_checkpoint]
        #    validation_data=_batch_generator_testing(),
        #    # Should be the number of samples divided by the batch size.
        #    validation_steps=int(limit_testing / BATCH_SIZE),
        #    class_weight=None,
        #    max_queue_size=10,
        ##   workers=0,
        #   use_multiprocessing=False,
        #   shuffle=True,
        ##  initial_epoch=step * epochs)

        # history =
        epochs = 1
        model.fit(
            x=x_train, y=gan_targets(x_train.shape[0]),
            validation_data=(x_test, gan_targets(x_test.shape[0])),
            callbacks=[tensorboard],
            batch_size=1024,
            epochs=step + epochs,  # read: final epoch
            initial_epoch=step * epochs,
            verbose=1,
            shuffle=False,
            steps_per_epoch=None,
        )

        # csv_path = os.path.join(CURRENT_CHECKPOINT_PATH, "history.csv")

        if step % MODEL_SAVE_INTERVAL == 0:
            print("Saving models ...")
            generator.save(os.path.join(CURRENT_CHECKPOINT_PATH, 'generator_step_' + str(step) + '.h5'))
            discriminator.save(os.path.join(CURRENT_CHECKPOINT_PATH, 'discriminator_step_' + str(step) + '.h5'))
            print("Saved models.")


if __name__ == "__main__":
    _main()
