from pfasdr.experiments.SeqGAN.config import COMMON_EMBEDDINGS_COUNT
from pfasdr.grid_gridcal.sampler.c_plot_samples_util import get_valid_powers_from_situations
from pfasdr.util.paths import DATA_REAL_FILE_PATH


def _parse_csv_file_to_list():
    print('Parsing valid powers ...')
    powers_valid = get_valid_powers_from_situations(limit=1000000)
    sample_count_valid = len(powers_valid)
    print('Valid sample count', sample_count_valid)

    for index in range(powers_valid.shape[0]):
        line = []
        if not index % 10000:
            print(index)  # , end='\r')

        line.append(float(powers_valid['power0'].iloc[index]))
        line.append(float(powers_valid['power1'].iloc[index]))
        line.append(float(powers_valid['power2'].iloc[index]))
        line.append(float(powers_valid['power3'].iloc[index]))
        line.append(float(powers_valid['power4'].iloc[index]))
        line.append(float(powers_valid['power5'].iloc[index]))
        line.append(float(powers_valid['power6'].iloc[index]))

        # line_parsed_to_list = [int(x) for x in line]

        max_power = 20  # per circuit sampling
        max_integer = 2 ** 63 - 1  # 64 bit integer
        integers = [int(COMMON_EMBEDDINGS_COUNT * (power / max_power * max_integer) / max_integer)
                    for power in line]
        strings = [str(power)
                   for power in integers]
        line = ' '.join(strings)
        line += '\n'

        yield line


def copy_valid_files_to_real_data():
    print(f'Writing to {DATA_REAL_FILE_PATH}')
    with open(DATA_REAL_FILE_PATH, 'w') as out_file:
        out_file.writelines(
            _parse_csv_file_to_list()
        )


if __name__ == '__main__':
    copy_valid_files_to_real_data()
