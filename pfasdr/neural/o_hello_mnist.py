import tensorflow as tf

(x_train, y_train), (x_test, y_test) = tf.python.keras.datasets.mnist.load_data()

x_train, x_test = x_train / 255, x_test / 255

model = tf.python.keras.models.Sequential([
  tf.python.keras.layers.Flatten(),
  tf.python.keras.layers.Dense(512, activation=tf.nn.relu),
  tf.python.keras.layers.Dropout(0.2),
  tf.python.keras.layers.Dense(10, activation=tf.nn.softmax)
])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=5)

model.evaluate(x_test, y_test)
