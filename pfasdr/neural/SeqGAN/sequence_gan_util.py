import random

import numpy
import tensorflow

from pfasdr.neural.SeqGAN.config import BATCH_SIZE, GENERATOR_SAMPLE_COUNT, SEED
from pfasdr.util.paths import DATA_EVAL_FILE_PATH, DATA_REAL_FILE_PATH, DATA_FAKE_FILE_PATH


def test_generator(generator, session, target_lstm, target_lstm_data_loader):
    sample_evaluation_data_from_generator(generator, session)
    target_lstm_data_loader.create_batches_from_evaluation_data()
    testing_loss = _target_loss(session, target_lstm, target_lstm_data_loader)
    return testing_loss

def _target_loss(session, target_lstm, data_loader):
    # target_loss means the oracle negative log-likelihood tested with the
    # oracle neural "target_lstm"
    # For more details, please see the Section 4 in
    # https://arxiv.org/abs/1609.05473
    nll = []
    data_loader.reset_pointer()

    for _ in range(data_loader.batch_count):
        batch = data_loader.next_batch()
        g_loss = session.run(target_lstm.pretrain_loss, {target_lstm.x: batch})
        nll.append(g_loss)

    return numpy.mean(nll)


def sample_evaluation_data_from_generator(generator, session):
    _generate_samples(session=session,
                      trainable_model=generator,
                      batch_size=BATCH_SIZE,
                      generated_num=GENERATOR_SAMPLE_COUNT,
                      output_file_path=DATA_EVAL_FILE_PATH)


def sample_real_data_from_oracle(session, target_lstm):
    _generate_samples(session=session,
                      trainable_model=target_lstm,
                      batch_size=BATCH_SIZE,
                      generated_num=GENERATOR_SAMPLE_COUNT,
                      output_file_path=DATA_REAL_FILE_PATH)


def sample_fake_data_from_generator(generator, session):
    _generate_samples(session=session,
                      trainable_model=generator,
                      batch_size=BATCH_SIZE,
                      generated_num=GENERATOR_SAMPLE_COUNT,
                      output_file_path=DATA_FAKE_FILE_PATH)


def _generate_samples(session, trainable_model, batch_size, generated_num,
                      output_file_path):
    generated_samples = []
    for _ in range(int(generated_num / batch_size)):
        generated_samples.extend(trainable_model.generate(session))

    with open(output_file_path, 'w') as fout:
        for poem in generated_samples:
            buffer = ' '.join([str(x) for x in poem]) + '\n'
            fout.write(buffer)


def pretrain_epoch(sess, trainable_model, data_loader):
    """
    Pre-train the given model using MLE
    """
    supervised_g_losses = []
    data_loader.reset_pointer()

    for _ in range(data_loader.batch_count):
        batch = data_loader.next_batch()
        _, g_loss = trainable_model.pretrain_step(sess, batch)
        supervised_g_losses.append(g_loss)

    return numpy.mean(supervised_g_losses)


def set_seeds():
    """
    Set all seeds
    """
    random.seed(SEED)
    numpy.random.seed(SEED)
    tensorflow.set_random_seed(SEED)
