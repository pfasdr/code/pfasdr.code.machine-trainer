import tensorflow as tf
from tensorflow.python.client.session import Session

from pfasdr.neural.v_wgan_gp import WassersteinGanGp
from pfasdr.neural.v_wgan_gp import show_all_variables

if __name__ == '__main__':
    # Move optimizers to CPU
    # TODO This is necessary because of the gfx803 regression since November 2018
    # TODO This makes training way slower
    config = tf.ConfigProto(device_count={'GPU': 0}, allow_soft_placement=False)
    # with Session(config=config) as session:
    with Session() as session:
        epoch = 20
        batch_size = 64
        z_dim = 62
        dataset = 'mnist'
        checkpoint_dir = 'checkpoint'
        result_dir = 'results'
        log_dir = 'logs'
        gan = WassersteinGanGp(
            session=session,
            epoch=epoch,
            batch_size=batch_size,
            z_dim=z_dim,
            dataset_name=dataset,
            checkpoint_dir=checkpoint_dir,
            result_dir=result_dir,
            log_dir=log_dir
        )

        # build graph
        gan.build_model()

        # show network architecture
        show_all_variables()

        # launch the graph in a session
        gan.train()
        print(" [*] Training finished!")

        # visualize learned generator
        gan.visualize_results(epoch - 1)
        print(" [*] Testing finished!")
