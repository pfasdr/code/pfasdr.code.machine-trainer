import os

from pfasdr.neural.ze_discriminate_pd_np.log_dir_path_module import \
    logs_fit_dir

os.system(
    f'tensorboard --logdir={logs_fit_dir} --port=8008 --reload_multifile=true'
)
