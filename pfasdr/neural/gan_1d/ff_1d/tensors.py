from pfasdr.neural.w_gan_1d import CONVOLUTION_PADDING
from pfasdr.neural.w_gan_1d import PROBLEM_WIDTH, LATENT_LENGTH

from tensorflow.python.keras.layers.advanced_activations import LeakyReLU
from tensorflow.python.keras.layers import Dense, Dropout, Flatten, GaussianNoise, Input, Reshape
from tensorflow.python.keras.layers.convolutional import Conv1D

from share.keras_layers.conv_1D_transpose import Conv1DTranspose


def get_discriminator_tensors():
    y = Input(shape=(PROBLEM_WIDTH, ))

    x = GaussianNoise(stddev=0.2)(y)

    x = Conv1D(filters=PROBLEM_WIDTH ** 2, kernel_size=4, padding=CONVOLUTION_PADDING, strides=PROBLEM_WIDTH)(x)
    x = LeakyReLU(alpha=0.3)(x)
    x = Dropout(rate=0.25)(x)

    x = Conv1D(filters=PROBLEM_WIDTH ** 3, kernel_size=4, padding=CONVOLUTION_PADDING, strides=PROBLEM_WIDTH)(x)
    x = LeakyReLU(alpha=0.3)(x)
    x = Dropout(rate=0.25)(x)

    x = Flatten()(x)

    x = Dense(units=PROBLEM_WIDTH ** 3)(x)
    x = LeakyReLU(alpha=0.3)(x)
    x = Dropout(rate=0.25)(x)

    x = Dense(units=1, activation='softmax')(x)

    return y, x


def get_generator_tensors():
    y = Input(shape=(LATENT_LENGTH,))

    x = Dense(units=PROBLEM_WIDTH ** 3)(y)
    x = LeakyReLU(alpha=0.3)(x)
    x = Dropout(rate=0.25)(x)

    x = Dense(units=PROBLEM_WIDTH ** 2)(x)
    x = LeakyReLU(alpha=0.3)(x)
    x = Dropout(rate=0.25)(x)

    # DeFlatten
    x = Reshape(target_shape=(PROBLEM_WIDTH, -1))(x)

    # Deconvolution
    x = Conv1DTranspose(filters=PROBLEM_WIDTH ** 1, kernel_size=4, padding=CONVOLUTION_PADDING, strides=1)(x)
    x = LeakyReLU(alpha=0.3)(x)
    x = Dropout(rate=0.25)(x)

    # Deconvolution
    x = Conv1DTranspose(filters=1, kernel_size=4, strides=1, padding=CONVOLUTION_PADDING, activation='tanh')(x)

    return y, x
