from typing import Union

import numpy

from numpy.core.multiarray import ndarray

# number of adversarial training steps
STEP_LIMIT = 10 ** 10

# size of the micro batches by which to train the networks in each update
BATCH_SIZE = 10 ** 4

# number of discriminator network updates per adversarial training step
UPDATES_PER_STEP_DISCRIMINATOR = 10

# number of generative network updates per adversarial training step
UPDATES_PER_STEP_GENERATOR = 2

# dimensions of the problem grid
PROBLEM_WIDTH = 7
CHANNEL_COUNT = 1
PROBLEM_SHAPE = (PROBLEM_WIDTH, CHANNEL_COUNT)

# interval in steps at which to log loss summaries to console
# interval in steps at which to save plots of image samples to disk
LOG_INTERVAL = 10

# fixed noise to generate batches of generated images
FIXED_NOISE = \
    numpy.random.normal(size=(BATCH_SIZE, PROBLEM_WIDTH))
MODEL_SAVE_INTERVAL = 2

# dimension of generator's input tensor (gaussian noise)
LATENT_LENGTH = PROBLEM_WIDTH
