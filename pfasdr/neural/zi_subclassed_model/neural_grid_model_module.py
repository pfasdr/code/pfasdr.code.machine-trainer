from tensorflow.python.keras import Model
from tensorflow.python.keras.layers import Dense, Dropout
from tensorflow.python.keras.regularizers import l2
from tensorflow.python.ops.gen_nn_ops import leaky_relu


class NeuralGridModel(Model):

    def get_config(self):
        raise NotImplementedError()

    def __init__(self, node_count):
        super(NeuralGridModel, self).__init__()

        self._dense1 = Dense(
            units=512,
            input_dim=node_count,
            activation=leaky_relu,
            kernel_regularizer=l2(l=0.0000001)
        )
        self._dropout1 = Dropout(rate=0.5)
        self._dense2 = Dense(
            units=256,
            activation=leaky_relu,
            kernel_regularizer=l2(l=0.0000001)
        )
        self._dropout2 = Dropout(rate=0.5)
        self._dense3 = Dense(
            units=256,
            activation=leaky_relu,
            kernel_regularizer=l2(l=0.0000001)
        )
        self._dropout3 = Dropout(rate=0.5)
        self._dense4 = Dense(
            units=1,
            activation='sigmoid',
            kernel_regularizer=l2(l=0.0000001)
        )

    def call(self, inputs, training=False, **kwargs):
        """
        This method uses a branch which is harder to predict.

        Use call training and call testing for non-branching variants.

        :param inputs:
        :param training:
        :param kwargs:
        :return:
        """
        if training:
            return self.call_training(inputs)
        else:
            return self.call_testing(inputs)

    def call_training(self, inputs):
        inputs = self._dense1(inputs)
        inputs = self._dropout1(inputs)
        inputs = self._dense2(inputs)
        inputs = self._dropout2(inputs)
        inputs = self._dense3(inputs)
        inputs = self._dropout3(inputs)
        inputs = self._dense4(inputs)

        return inputs

    def call_testing(self, inputs):
        inputs = self._dense1(inputs)
        inputs = self._dense2(inputs)
        inputs = self._dense3(inputs)
        inputs = self._dense4(inputs)

        return inputs
