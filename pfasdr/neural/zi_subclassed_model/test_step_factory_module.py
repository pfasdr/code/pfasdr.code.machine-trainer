from typing import Tuple

from tensorflow import function
from tensorflow.python.keras import Model


def test_step_factory(
    *,
    model: Model,
    test_loss,
    loss_function,
    test_accuracy,
    mirrored,
):
    if mirrored:
        @function(autograph=True)
        def test_step(*, dist_inputs: Tuple):
            print("Python execution: ", dist_inputs)
            print("Python execution: ", type(dist_inputs))

            features_testing, labels_testing = dist_inputs

            print("Python execution: ", features_testing)
            print("Python execution: ", type(features_testing))
            print("Python execution: ", labels_testing)
            print("Python execution: ", type(labels_testing))

            # TODO Parallelize dataset iteration
            for replica_index in range(len(features_testing.values)):
                replica_features_testing = \
                    features_testing.values[replica_index]
                replica_labels_testing = \
                    labels_testing.values[replica_index]

                predictions: int = model.call_testing(
                    replica_features_testing)

                test_loss(loss_function(replica_labels_testing, predictions))
                test_accuracy(replica_labels_testing, predictions)

    else:
        @function(autograph=True)
        def test_step(*, dist_inputs):
            # print("Python execution: ", dist_inputs)

            features_testing, labels_testing = dist_inputs

            predictions = model.call_testing(features_testing)

            test_loss(loss_function(labels_testing, predictions))
            test_accuracy(labels_testing, predictions)

    return test_step
