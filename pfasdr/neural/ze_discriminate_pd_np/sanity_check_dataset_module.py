def sanity_check_dataset(dataset):
    found_valid = False
    found_invalid = False
    for index, element in enumerate(dataset):
        print(index, element[1].numpy()[0])
        if element[1].numpy()[0]:
            found_valid = True
        else:
            found_invalid = True
        if found_valid and found_invalid:
            break
    if not found_valid:
        raise Exception('No valid sample found.')
    if not found_invalid:
        raise Exception('No invalid sample found.')
    print(f'Found valid and invalid sample after {index} elements.')
