"""Trains a GANEstimator on MNIST data."""

import gzip
import os
import sys
import urllib

import imageio
import numpy as np
import absl
import tensorflow as tf

from share.python_os.ensure_path import ensure_path

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

"""
Downloads and converts MNIST data to TFRecords of TF-Example protos.

This module downloads the MNIST data, uncompresses it, reads the files
that make up the MNIST data and creates two TFRecord datasets: one for train
and one for test. Each TFRecord dataset is comprised of a set of TF-Example
protocol buffers, each of which contain a single image and label.

The script should take about a minute to run.
"""

_FILE_PATTERN = 'mnist_%s.tfrecord'

_SPLITS_TO_SIZES = {'train': 60000, 'test': 10000}

_NUM_CLASSES = 10

_ITEMS_TO_DESCRIPTIONS = {
    'image': 'A [28 x 28 x 1] greyscale image.',
    'label': 'A single integer between 0 and 9',
}

LABELS_FILENAME = 'labels.txt'


def read_label_file(*,
                    dataset_dir, filename=LABELS_FILENAME):
    """
    Reads the labels file and returns a mapping from ID to class name.

    Args:
        dataset_dir: The directory in which the labels file is found.
        filename: The filename where the class names are written.

    Returns:
        A map from a label (integer) to class name.
    """
    labels_filename = os.path.join(dataset_dir, filename)
    with open(labels_filename, 'rb') as label_file:
        lines = label_file.read().decode()
    lines = lines.split('\n')
    lines = filter(None, lines)

    labels_to_class_names = {}
    for line in lines:
        index = line.index(':')
        labels_to_class_names[int(line[:index])] = line[index + 1:]

    return labels_to_class_names


def has_labels(dataset_dir, filename=LABELS_FILENAME):
    """
    Specifies whether or not the dataset directory contains a label map file.

    Args:
        dataset_dir: The directory in which the labels file is found.
        filename: The filename where the class names are written.

    Returns:
        `True` if the labels file exists and `False` otherwise.
    """
    return tf.gfile.Exists(os.path.join(dataset_dir, filename))


def get_split(split_name, dataset_dir, file_pattern=None, reader=None):
    """
    Gets a dataset tuple with instructions for reading MNIST.

    Args:
        split_name: A train/test split name.
        dataset_dir: The base directory of the dataset sources.
        file_pattern: The file pattern to use when matching the dataset sources.
        It is assumed that the pattern contains a '%s' string so that the split
        name can be inserted.
        reader: The TensorFlow reader type.

    Returns:
        A `Dataset` namedtuple.

    Raises:
        ValueError: if `split_name` is not a valid train/test split.
    """
    if split_name not in _SPLITS_TO_SIZES:
        raise ValueError('split name %s was not recognized.' % split_name)

    if not file_pattern:
        file_pattern = _FILE_PATTERN
    file_pattern = os.path.join(dataset_dir, file_pattern % split_name)

    # Allowing None in the signature so that dataset_factory can use the default.
    if reader is None:
        reader = tf.TFRecordReader

    keys_to_features = {
        'image/encoded': tf.FixedLenFeature((), tf.string, default_value=''),
        'image/format': tf.FixedLenFeature((), tf.string, default_value='raw'),
        'image/class/label': tf.FixedLenFeature(
            [1], tf.int64, default_value=tf.zeros([1], dtype=tf.int64)),
    }

    items_to_handlers = {
        'image': tf.contrib.slim.tfexample_decoder.Image(shape=[28, 28, 1], channels=1),
        'label': tf.contrib.slim.tfexample_decoder.Tensor('image/class/label', shape=[]),
    }

    decoder = tf.contrib.slim.tfexample_decoder.TFExampleDecoder(
        keys_to_features, items_to_handlers)

    labels_to_names = None
    if has_labels(dataset_dir):
        labels_to_names = read_label_file(dataset_dir=dataset_dir)

    dataset = tf.contrib.slim.dataset.Dataset(
        data_sources=file_pattern,
        decoder=decoder,
        items_to_descriptions=_ITEMS_TO_DESCRIPTIONS,
        labels_to_names=labels_to_names,
        num_samples=_SPLITS_TO_SIZES[split_name], num_classes=_NUM_CLASSES,
        reader=reader,
    )
    return dataset


# The URLs where the MNIST data can be downloaded.
_DATA_URL = 'http://yann.lecun.com/exdb/mnist/'
_TRAIN_DATA_FILENAME = 'train-images-idx3-ubyte.gz'
_TRAIN_LABELS_FILENAME = 'train-labels-idx1-ubyte.gz'
_TEST_DATA_FILENAME = 't10k-images-idx3-ubyte.gz'
_TEST_LABELS_FILENAME = 't10k-labels-idx1-ubyte.gz'

_IMAGE_SIZE = 28
_NUM_CHANNELS = 1

# The names of the classes.
_CLASS_NAMES = [
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'size',
    'seven',
    'eight',
    'nine',
]


def _extract_images(filename, num_images):
    """
    Extract the images into a numpy array.

    Args:
        filename: The path to an MNIST images file.
        num_images: The number of images in the file.

    Returns:
        A numpy array of shape [number_of_images, height, width, channels].
    """
    print('Extracting images from: ', filename)
    with gzip.open(filename) as bytestream:
        bytestream.read(16)
        buf = bytestream.read(
            _IMAGE_SIZE * _IMAGE_SIZE * num_images * _NUM_CHANNELS)
        data = np.frombuffer(buf, dtype=np.uint8)
        data = data.reshape(num_images, _IMAGE_SIZE, _IMAGE_SIZE, _NUM_CHANNELS)
    return data


def _extract_labels(filename, num_labels):
    """
    Extract the labels into a vector of int64 label IDs.

    Args:
        filename: The path to an MNIST labels file.
        num_labels: The number of labels in the file.

    Returns:
        A numpy array of shape [number_of_labels]
    """
    print('Extracting labels from: ', filename)
    with gzip.open(filename) as bytestream:
        bytestream.read(8)
        buf = bytestream.read(1 * num_labels)
        labels = np.frombuffer(buf, dtype=np.uint8).astype(np.int64)
    return labels


def _add_to_tfrecord(data_filename, labels_filename, num_images,
                     tfrecord_writer):
    """
    Loads data from the binary MNIST files and writes files to a TFRecord.

    Args:
        data_filename: The filename of the MNIST images.
        labels_filename: The filename of the MNIST labels.
        num_images: The number of images in the dataset.
        tfrecord_writer: The TFRecord writer to use for writing.
    """
    images = _extract_images(data_filename, num_images)
    labels = _extract_labels(labels_filename, num_images)

    shape = (_IMAGE_SIZE, _IMAGE_SIZE, _NUM_CHANNELS)
    with tf.Graph().as_default():
        image = tf.placeholder(dtype=tf.uint8, shape=shape)
        encoded_png = tf.image.encode_png(image)

        with tf.Session('') as session:
            for j in range(num_images):
                sys.stdout.write('\r>> Converting image %d/%d' % (j + 1, num_images))
                sys.stdout.flush()

                png_string = session.run(encoded_png, feed_dict={image: images[j]})

                example = _image_to_tf_example(
                    png_string, 'png'.encode(), _IMAGE_SIZE, _IMAGE_SIZE, labels[j])
                tfrecord_writer.write(example.SerializeToString())


def int64_feature(values):
    """
    Returns a TF-Feature of int64s.

    Args:
        values: A scalar or list of values.

    Returns:
        A TF-Feature.
    """
    if not isinstance(values, (tuple, list)):
        values = [values]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=values))


def bytes_feature(values):
    """
    Returns a TF-Feature of bytes.

    Args:
        values: A string.

    Returns:
        A TF-Feature.
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[values]))


def _image_to_tf_example(image_data, image_format, height, width, class_id):
    return tf.train.Example(features=tf.train.Features(feature={
        'image/encoded': bytes_feature(image_data),
        'image/format': bytes_feature(image_format),
        'image/class/label': int64_feature(class_id),
        'image/height': int64_feature(height),
        'image/width': int64_feature(width),
    }))


def _get_output_filename(*,
                         dataset_dir, split_name):
    """Creates the output filename.

  Args:
    dataset_dir: The directory where the temporary files are stored.
    split_name: The name of the train/test split.

  Returns:
    An absolute file path.
  """
    return '%s/mnist_%s.tfrecord' % (dataset_dir, split_name)


def _download_dataset(*,
                      dataset_dir):
    """Downloads MNIST locally.

  Args:
    dataset_dir: The directory where the temporary files are stored.
  """
    for filename in [_TRAIN_DATA_FILENAME,
                     _TRAIN_LABELS_FILENAME,
                     _TEST_DATA_FILENAME,
                     _TEST_LABELS_FILENAME]:
        filepath = os.path.join(dataset_dir, filename)

        if not os.path.exists(filepath):
            print('Downloading file %s...' % filename)

            def _progress(count, block_size, total_size):
                sys.stdout.write('\r>> Downloading %.1f%%' % (
                        float(count * block_size) / float(total_size) * 100.0))
                sys.stdout.flush()

            filepath, _ = urllib.request.urlretrieve(
                _DATA_URL + filename,
                filepath,
                _progress,
            )
            #with open(filepath) as f:
            #    size = f.size()

            print()
            print('Successfully downloaded', filename)  # , size, 'bytes.')


def _clean_up_temporary_files(*,
                              dataset_dir):
    """Removes temporary files used to create the dataset.

  Args:
    dataset_dir: The directory where the temporary files are stored.
  """
    for filename in [_TRAIN_DATA_FILENAME,
                     _TRAIN_LABELS_FILENAME,
                     _TEST_DATA_FILENAME,
                     _TEST_LABELS_FILENAME]:
        filepath = os.path.join(dataset_dir, filename)
        os.remove(filepath)


def download_and_convert(*,
                         dataset_dir):
    """
    Runs the download and conversion operation.

    Args:
        dataset_dir: The dataset directory where the dataset is stored.
    """
    ensure_path(path=dataset_dir)

    training_filename = _get_output_filename(dataset_dir=dataset_dir,
                                             split_name='train')
    testing_filename = _get_output_filename(dataset_dir=dataset_dir,
                                            split_name='test')

    if tf.gfile.Exists(training_filename) and tf.gfile.Exists(testing_filename):
        print('Dataset files already exist. Exiting without re-creating them.')
        return

    _download_dataset(dataset_dir=dataset_dir)

    # First, process the training data:
    with tf.python_io.TFRecordWriter(training_filename) as tfrecord_writer:
        data_filename = os.path.join(dataset_dir, _TRAIN_DATA_FILENAME)
        labels_filename = os.path.join(dataset_dir, _TRAIN_LABELS_FILENAME)
        _add_to_tfrecord(data_filename, labels_filename, 60000, tfrecord_writer)

    # Next, process the testing data:
    with tf.python_io.TFRecordWriter(testing_filename) as tfrecord_writer:
        data_filename = os.path.join(dataset_dir, _TEST_DATA_FILENAME)
        labels_filename = os.path.join(dataset_dir, _TEST_LABELS_FILENAME)
        _add_to_tfrecord(data_filename, labels_filename, 10000, tfrecord_writer)

    # Finally, write the labels file:
    labels_to_class_names = dict(zip(range(len(_CLASS_NAMES)), _CLASS_NAMES))
    write_label_file(
        labels_to_class_names=labels_to_class_names,
        dataset_dir=dataset_dir,
    )

    _clean_up_temporary_files(dataset_dir=dataset_dir)
    print('\nFinished converting the MNIST dataset!')


def write_label_file(*,
                     labels_to_class_names, dataset_dir,
                     filename=LABELS_FILENAME):
    """
    Writes a file with the list of class names.

    Args:
        labels_to_class_names: A map of (integer) labels to class names.
        dataset_dir: The directory in which the labels file should be written.
        filename: The filename where the class names are written.
    """
    labels_filename = os.path.join(dataset_dir, filename)
    with tf.gfile.Open(labels_filename, 'w') as f:
        for label in labels_to_class_names:
            class_name = labels_to_class_names[label]
            f.write('%d:%s\n' % (label, class_name))


def get_dataset(*,
                _, split_name, dataset_dir, file_pattern=None, reader=None):
    """
    Given a dataset name and a split_name returns a Dataset.

    Args:
        split_name: A train/test split name.
        dataset_dir: The directory where the dataset files are stored.
        file_pattern: The file pattern to use for matching the dataset source files.
        reader: The subclass of tf.ReaderBase. If left as `None`, then the default
        reader defined by each dataset is used.

    Returns:
        A `Dataset` class.

    Raises:
        ValueError: If the dataset `name` is unknown.
    """
    return get_split(
        split_name,
        dataset_dir,
        file_pattern,
        reader)


def _generator_helper(
        noise, is_conditional, one_hot_labels, weight_decay, is_training):
    """
    Core MNIST generator.

    This function is reused between the different GAN modes (unconditional,
    conditional, etc).

    Args:
        noise: A 2D Tensor of shape [batch size, noise dim].
        is_conditional: Whether to condition on labels.
        one_hot_labels: Optional labels for conditioning.
        weight_decay: The value of the l2 weight decay.
        is_training: If `True`, batch norm uses batch statistics. If `False`, batch
          norm uses the exponential moving average collected from population
          statistics.

    Returns:
        A generated image in the range [-1, 1].
    """
    with tf.contrib.framework.arg_scope(
            [tf.contrib.layers.fully_connected, tf.contrib.layers.conv2d_transpose],
            activation_fn=tf.nn.relu,
            normalizer_fn=tf.contrib.layers.batch_norm,
            weights_regularizer=tf.contrib.layers.l2_regularizer(weight_decay),
    ):
        with tf.contrib.framework.arg_scope(
                [tf.contrib.layers.batch_norm], is_training=is_training
        ):
            net = tf.contrib.layers.fully_connected(noise, 1024)
            if is_conditional:
                net = tf.contrib.gan.features.condition_tensor_from_onehot(net, one_hot_labels)
            net = tf.contrib.layers.fully_connected(net, 7 * 7 * 128)
            net = tf.reshape(net, [-1, 7, 7, 128])
            net = tf.contrib.layers.conv2d_transpose(net, 64, [4, 4], stride=2)
            net = tf.contrib.layers.conv2d_transpose(net, 32, [4, 4], stride=2)
            # Make sure that generator output is in the same range as `inputs`
            # ie [-1, 1].
            net = tf.contrib.layers.conv2d(
                net, 1, [4, 4],
                normalizer_fn=None,
                activation_fn=tf.tanh,
            )

            return net


def unconditional_generator(noise, weight_decay=2.5e-5, is_training=True):
    """Generator to produce unconditional MNIST images.

  Args:
    noise: A single Tensor representing noise.
    weight_decay: The value of the l2 weight decay.
    is_training: If `True`, batch norm uses batch statistics. If `False`, batch
      norm uses the exponential moving average collected from population
      statistics.

  Returns:
    A generated image in the range [-1, 1].
  """
    return _generator_helper(noise, False, None, weight_decay, is_training)


def _leaky_relu(x):
    return tf.nn.leaky_relu(x, alpha=0.01)


def _discriminator_helper(img, is_conditional, one_hot_labels, weight_decay):
    """Core MNIST discriminator.

  This function is reused between the different GAN modes (unconditional,
  conditional, etc).

  Args:
    img: Real or generated MNIST digits. Should be in the range [-1, 1].
    is_conditional: Whether to condition on labels.
    one_hot_labels: Labels to optionally condition the network on.
    weight_decay: The L2 weight decay.

  Returns:
    Final fully connected discriminator layer. [batch_size, 1024].
  """
    with tf.contrib.framework.arg_scope(
            [tf.contrib.layers.conv2d, tf.contrib.layers.fully_connected],
            activation_fn=_leaky_relu, normalizer_fn=None,
            weights_regularizer=tf.contrib.layers.l2_regularizer(weight_decay),
            biases_regularizer=tf.contrib.layers.l2_regularizer(weight_decay)):
        net = tf.contrib.layers.conv2d(img, 64, [4, 4], stride=2)
        net = tf.contrib.layers.conv2d(net, 128, [4, 4], stride=2)
        net = tf.python.keras.layers.Flatten()(net)
        if is_conditional:
            net = tf.contrib.gan.features.condition_tensor_from_onehot(net, one_hot_labels)
        net = tf.contrib.layers.fully_connected(net, 1024, normalizer_fn=tf.contrib.layers.layer_norm)

        return net


def unconditional_discriminator(img, unused_conditioning, weight_decay=2.5e-5):
    """Discriminator network on unconditional MNIST digits.

  Args:
    img: Real or generated MNIST digits. Should be in the range [-1, 1].
    unused_conditioning: The TFGAN API can help with conditional GANs, which
      would require extra `condition` information to both the generator and the
      discriminator. Since this example is not conditional, we do not use this
      argument.
    weight_decay: The L2 weight decay.

  Returns:
    Logits for the probability that the image is real.
  """
    net = _discriminator_helper(img, False, None, weight_decay)
    net = tf.contrib.layers.linear(net, 1)

    return net


def provide_data(split_name, batch_size, dataset_dir, num_readers=1,
                 num_threads=1):
    """Provides batches of MNIST digits.

  Args:
    split_name: Either 'train' or 'test'.
    batch_size: The number of images in each batch.
    dataset_dir: The directory where the MNIST data can be found.
    num_readers: Number of dataset readers.
    num_threads: Number of prefetching threatf.contrib.distributions.

  Returns:
    images: A `Tensor` of size [batch_size, 28, 28, 1]
    one_hot_labels: A `Tensor` of size [batch_size, mnist.NUM_CLASSES], where
      each row has a single element set to one and the rest set to zeros.
    num_samples: The number of total samples in the dataset.

  Raises:
    ValueError: If `split_name` is not either 'train' or 'test'.
  """
    dataset = get_dataset(
        _=None,
        split_name=split_name,
        dataset_dir=dataset_dir,
    )
    provider = tf.contrib.slim.dataset_data_provider.DatasetDataProvider(
        dataset,
        num_readers=num_readers,
        common_queue_capacity=2 * batch_size,
        common_queue_min=batch_size,
        shuffle=(split_name == 'train'),
    )
    [image, label] = provider.get(['image', 'label'])

    # Preprocess the images.
    image = (tf.to_float(image) - 128.0) / 128.0

    # Creates a QueueRunner for the pre-fetching operation.
    images, labels = tf.train.batch(
        [image, label],
        batch_size=batch_size,
        num_threads=num_threads,
        capacity=5 * batch_size,
    )

    one_hot_labels = tf.one_hot(labels, dataset.num_classes)
    return images, one_hot_labels, dataset.num_samples


absl.flags.DEFINE_integer(
    'batch_size',
    32,
    'The number of images in each train batch.'
)
absl.flags.DEFINE_integer(
    'max_number_of_steps',
    2000,
    'The maximum number of gradient steps.'
)

absl.flags.DEFINE_integer(
    'noise_dims',
    64,
    'Dimensions of the generator noise vector'
)

absl.flags.DEFINE_string(
    'eval_dir',
    '/tmp/mnist-estimator/',
    'Directory where the results are saved to.'
)

absl.flags.DEFINE_string(
    'dataset_name',
    'mnist',
    'The name of the dataset to convert, one of "cifar10", "flowers", "mnist".'
)

absl.flags.DEFINE_string(
    'dataset_dir',
    '/tmp/mnist-data',
    'The directory where the output TFRecords and temporary files are saved.'
)


def _get_train_input_fn(batch_size, noise_dims, dataset_dir,
                        num_threads=4):
    def _train_input_fn():
        with tf.device('/cpu:0'):
            images, _, _ = provide_data(
                'train',
                batch_size,
                dataset_dir,
                num_threads=num_threads,
            )
        noise = tf.random_normal([batch_size, noise_dims])
        return noise, images

    return _train_input_fn


def _get_predict_input_fn(*, batch_size, noise_dims):
    def _predict_input_fn():
        noise = tf.random_normal([batch_size, noise_dims])
        return noise

    return _predict_input_fn


def _unconditional_generator(noise, mode):
    """
    MNIST generator with extra argument for tf.Estimator's `mode`.
    """
    is_training = (mode == tf.estimator.ModeKeys.TRAIN)
    return unconditional_generator(noise, is_training=is_training)


def _convert_image_to_uint8(*, image, bit_depth=8):
    smallest = np.nanmin(image)
    if not np.isfinite(smallest):
        raise ValueError("Minimum image value is not finite")
    largest = np.nanmax(image)
    if not np.isfinite(largest):
        raise ValueError("Maximum image value is not finite")
    if largest == smallest:
        raise ValueError("Max value == min value, ambiguous given dtype")

    image = image.astype("float64")
    # Scale the values between 0 and 1 then multiply by the max value
    image = (image - smallest) / (largest - smallest) * (np.power(2.0, bit_depth) - 1) + 0.499999999

    assert np.nanmin(image) >= 0
    assert np.nanmax(image) < np.power(2.0, bit_depth)

    image = image.astype(np.uint8)

    return image


def main(_):
    """Entry point for tensorflow app run."""

    # Initialize GANEstimator with options and hyperparameters.
    generator_optimizer = tf.train.AdamOptimizer(0.001, 0.5)
    # generator_optimizer = tf.train.RMSPropOptimizer(0.001, 0.5)
    # generator_optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)
    # generator_optimizer = tf.train.MomentumOptimizer(learning_rate=0.01, momentum=0.01)
    discriminator_optimizer = tf.train.AdamOptimizer(0.0001, 0.5)
    # discriminator_optimizer = tf.train.RMSPropOptimizer(0.0001, 0.5)
    # discriminator_optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)
    # discriminator_optimizer = tf.train.MomentumOptimizer(learning_rate=0.01, momentum=0.01)
    gan_estimator = tf.contrib.gan.estimator.GANEstimator(
        generator_fn=_unconditional_generator,
        discriminator_fn=unconditional_discriminator,
        generator_loss_fn=tf.contrib.gan.losses.wasserstein_generator_loss,
        discriminator_loss_fn=tf.contrib.gan.losses.wasserstein_discriminator_loss,
        generator_optimizer=generator_optimizer,
        discriminator_optimizer=discriminator_optimizer,
        add_summaries=tf.contrib.gan.estimator.SummaryType.IMAGES
    )

    # Train estimator.
    FLAGS = absl.flags.FLAGS
    train_input_fn = _get_train_input_fn(
        FLAGS.batch_size,
        FLAGS.noise_dims,
        FLAGS.dataset_dir,
    )
    gan_estimator.train(
        train_input_fn,
        max_steps=FLAGS.max_number_of_steps
    )

    # Run inference.
    predict_input_fn = _get_predict_input_fn(batch_size=36, noise_dims=FLAGS.noise_dims)
    prediction_iterable = gan_estimator.predict(predict_input_fn)
    predictions = [next(prediction_iterable) for _ in range(36)]

    # Nicely tile.
    image_rows = [
        np.concatenate(predictions[i:i + 6], axis=0) for i in range(0, 36, 6)
    ]
    image = np.concatenate(
        image_rows,
        axis=1,
    )

    # convert image to 8 bit per pixel
    image = _convert_image_to_uint8(image=image)

    # Reduce one-dimensional items in array
    image = np.squeeze(image, axis=2)

    # Write to disk.
    ensure_path(FLAGS.eval_dir)
    imageio.imwrite(
        uri=os.path.join(FLAGS.eval_dir, 'unconditional_gan.png'),
        im=image,
    )


if __name__ == '__main__':
    download_and_convert(dataset_dir='/tmp/mnist-data')

    tf.app.run()
