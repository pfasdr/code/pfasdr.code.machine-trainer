import os

import numpy as np

from pfasdr.neural.ze_discriminate_pd_np.mask_devices import mask_devices

np.set_printoptions(precision=3, suppress=True)

from multiprocessing import Process

from pfasdr.neural.ze_discriminate_pd_np.learn_grid_constraints_module import \
    learn_grid_constraints
from pfasdr.neural.ze_discriminate_pd_np.path_templates_module import \
    valid_file_path_template, invalid_file_path_template


def main():
    os.environ['AUTOGRAPH_VERBOSITY'] = '10'  # For reporting autograph bugs

    # mask_devices(visible=None)                  # CPU only
    mask_devices(visible=('0', '1', '2', '3'))  # All GPUs
    # mask_devices(visible=('0', '1'))
    # 0, 1, 2 hangs. ==> 3 is not the sole cause.
    # 0, 1 crashes after 20_313 epochs

    # batch_size_training = 4194304
    # batch_size_training = 2097152
    # batch_size_training = 1048576
    # batch_size_training = 524288    #
    # batch_size_training = 524288    # 4 GPU: 70 ... 83 ms/step 100 % CPU
    # batch_size_training = 262144    # Good for 15 node grid
    # batch_size_training = 131072   # Good for 14 ... 11 node grid
    batch_size_training = 65536   # Not good at all
    # batch_size_training = 32768   # 10 nodes: Train for 1 step, 4s/step
    # batch_size_training = 16384   # 10 nodes: Train for 1 step, 2 s/step
    # batch_size_training = 8192   # 10 nodes: Train for 1 step, 900 ms/step
    # batch_size_training = 4096   #
    # batch_size_training = 2048   #
    # batch_size_training = 1024   #
    # batch_size_training = 512    #
    # batch_size_training = 256    #
    # batch_size_training = 128    #
    # batch_size_training = 64     #
    # batch_size_training = 32
    # batch_size_training = 16
    # batch_size_training = 8
    # batch_size_training = 4
    # batch_size_training = 2
    # batch_size_training = 1

    epochs_training = 1_000_000

    for node_count in range(100, 99, -1):  # [1, 16)
        print('node_count', node_count)

        valid_file_path = valid_file_path_template.substitute(
            length=node_count,
        )
        invalid_file_path = invalid_file_path_template.substitute(
            length=node_count,
        )

        def target():
            learn_grid_constraints(
                node_count=node_count,
                batch_size=batch_size_training,
                epochs_training=epochs_training,
                invalid_file_path=invalid_file_path,
                valid_file_path=valid_file_path,
            )

        # process = Process(target=target)
        # process.start()
        # process.join()

        target()


if __name__ == '__main__':
    main()
