import os
from pathlib import Path

from pfasdr.util.paths import ROOT_PATH

grid_path = ROOT_PATH / 'data' / 'pandapower' / 'linear_grid'
for root, dirs, files in os.walk(grid_path):
    for file in files:
        root = Path(root)
        file_path = root / file
        print(file_path)

        file_path_dedup = str(file_path) + '.dedup'
        with \
                open(file_path, 'r') as in_file,\
                open(file_path_dedup, 'w') as out_file:
            seen = set()  # set for fast O(1) amortized lookup
            for line in in_file:
                if line in seen:
                    print('skip')
                    continue

                seen.add(line)
                out_file.write(line)

        os.remove(file_path)

        os.rename(file_path_dedup, file_path)
