FROM ubuntu:20.04

MAINTAINER Bengt Lüers "bengt.lueers@gmail.com"

RUN \
    apt update && \
    apt install --yes --quiet --quiet --no-install-recommends \
        software-properties-common && \
    add-apt-repository --yes ppa:deadsnakes/ppa && \
    apt update && \
    apt install --yes --quiet --quiet \
        # Install Python Interpreter
        python3.7-venv \
        python3-pip

COPY . /app
WORKDIR /app

RUN python3.7 -m venv venv
RUN venv/bin/python -m pip install --no-cache-dir -r requirements.d/venv.txt

RUN venv/bin/python -m tox -e py37 --notest

ENV PYTHONPATH=.
CMD .tox/py37/bin/python ./pfasdr/cli/main.py
