from setuptools import setup, find_packages

setup(
    name='pfasdr',
    version='0.0.0',
    url='https://gitlab.com/bengt/pfasdr',
    author='Bengt Lüers',
    author_email='bengt.lueers@gmail.com',
    description='Power Flow Aware Scheduling of Distributed (Energy) Resources',
    packages=find_packages(),
    install_requires=[
        # 'GridCal',
        # 'imageio',
        # 'keras',
        # 'matplotlib',
        # 'numpy>=1.18.1',
        # 'pandas',
        # 'pip',
        # 'scikit-learn',
        # 'seaborn',
        # 'setuptools',
        # 'wheel',
        # 'scipy',
        # 'tensorflow-rocm>=2.1.0',
        # 'gast==0.2.2',  # For Autograph
        # 'click>=7.1.1',
        # 'pandapower',
    ],
    tests_require=[
        'pytest'
    ],
)
